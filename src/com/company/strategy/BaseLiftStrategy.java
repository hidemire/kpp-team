package com.company.strategy;

import com.company.states.ILiftStrategy;
import com.company.states.Lift;
import com.company.states.Passenger;

import java.util.Map;

public abstract class BaseLiftStrategy implements ILiftStrategy {
    String Name = "";

    protected Integer direction = 1;
    protected Map.Entry<Integer, Passenger> nextPassenger;
    protected Map.Entry<Integer, Passenger> currentCall;

    @Override
      public Integer getCurrentCall() {
      if (nextPassenger != null) {
        return this.nextPassenger.getKey();
      }
      if (currentCall != null) {
        return this.currentCall.getKey();
      }
      return 0;
    }

    protected Integer getDirection(Lift lift, Integer floor) {
      Integer currentFloor = lift.getCurrentFloor();

      if (currentFloor > floor) return -1;
      if (currentFloor < floor) return 1;
      return 0;
    }

    @Override
    public String getName() {
      return Name;
    }
}
