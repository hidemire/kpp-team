package com.company.strategy;

import com.company.states.*;

public class OptimalLiftStrategy extends BaseLiftStrategy {
  public OptimalLiftStrategy() {
    this.Name = "Оптимальна стратегія";
  }

  @Override
  public void execute(Lift lift) {
    System.out.println("Load strategy move to");
    System.out.println(lift.getCalls());

    var calls = lift.getCalls();

    if (calls.size() == 0) {
      if (!lift.getIsLocked()) lift.setIsLocked(true);

      if (nextPassenger == null) {
        nextPassenger = lift.getEntrance().getNextPassenger();
        if (nextPassenger == null) return;
      }

      direction = getDirection(lift, nextPassenger.getKey());

      Integer moveTo = lift.getCurrentFloor() + direction;

      lift.MoveTo(moveTo, lift.getEntrance().getPassengersInsideLift());
      if (moveTo == nextPassenger.getKey()) {
        lift.setIsLocked(false);
        nextPassenger = null;
      }
    } else {
      if (!lift.getIsLocked()) lift.setIsLocked(true);

      if (currentCall == null) {
        currentCall = calls.poll();
      }

      direction = getDirection(lift, currentCall.getKey());

      Integer moveTo = lift.getCurrentFloor() + direction;

      lift.MoveTo(moveTo, lift.getEntrance().getPassengersInsideLift());
      if (moveTo == currentCall.getKey()) {
        currentCall = null;
      }
    }
  }
}
