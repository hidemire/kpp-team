package com.company.strategy;

import com.company.states.Lift;

public class LiftLoadStrategy extends BaseLiftStrategy {
  public LiftLoadStrategy() {
    this.Name = "Повільна стратегія";
  }

  @Override
  public void execute(Lift lift) {
    System.out.println("Load strategy move to");
    System.out.println(lift.getCalls());

    var calls = lift.getCalls();

    if (calls.size() == 0 && currentCall == null) {
      if (!lift.getIsLocked()) lift.setIsLocked(true);

      if (nextPassenger == null) {
        nextPassenger = lift.getEntrance().getNextPassenger();
        if (nextPassenger == null) return;
      }

      direction = getDirection(lift, nextPassenger.getKey());

      Integer moveTo = lift.getCurrentFloor() + direction;

      lift.MoveTo(moveTo, lift.getEntrance().getPassengersInsideLift());
      if (moveTo == nextPassenger.getKey()) {
        lift.setIsLocked(false);
        nextPassenger = null;
      }
    } else {
      if (!lift.getIsLocked()) lift.setIsLocked(true);

      if (currentCall == null) {
        currentCall = calls.poll();
      }

      direction = getDirection(lift, currentCall.getKey());

      Integer moveTo = lift.getCurrentFloor() + direction;

      lift.MoveTo(moveTo, lift.getEntrance().getPassengersInsideLift());
      if (moveTo == currentCall.getKey()) {
        currentCall = null;
      }
    }
  }
}
