package com.company.ui;

import com.company.states.Building;
import com.company.states.IBuildingBuilder;
import com.company.strategy.BaseLiftStrategy;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class UI extends JComponent {
  JFrame frame;
  UICanvas canvas;
  JTextField LiftsInput;
  JTextField FloorsInput;
  Building building;
  JButton StartButton;
  JLabel VaitingPassengers;
  JTextField PassengerSpawnTimer; //ввід часу генерування пасажира
  JRadioButton Strategy1RadioButton;
  JRadioButton Strategy2RadioButton;
  IBuildingBuilder Builder;
  ButtonGroup RadioGroup;

  boolean isRunnned = false;
  private AtomicBoolean running = new AtomicBoolean(false);
  private Map<String, Class<? extends BaseLiftStrategy>> strategies;

  public UI(IBuildingBuilder builder, Map<String, Class<? extends BaseLiftStrategy>> strategies) {
    this.Builder = builder;
    this.strategies = strategies;

    frame = new JFrame("Canvas");
    frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
    frame.setSize(900,600);
    frame.setLocationRelativeTo(null);
    frame.setLayout (null);

    canvas = new UICanvas (new Dimension (500, 550));
    canvas.setBorder (new LineBorder (Color.BLACK) {
    });
    //canvas.setSize (new Dimension (500,550));
    canvas.setBounds (5,5,500,550);
    frame.add (canvas);

    Font font = new Font ("Arial",Font.PLAIN,16);
    //column.setLayout (new BorderLayout ());
    StartButton= new JButton ("Розпочати симуляцію");
    StartButton.addActionListener (new ButtonAction ());
    StartButton.setSize (300,80);
    StartButton.setBorder (new EmptyBorder (10,10,10,10));
    StartButton.setBounds (550,450,300,80);
    StartButton.setFont (font);
    StartButton.setBorder (new LineBorder (Color.BLACK));

    Font font1 = new Font ("Arial",Font.PLAIN, 16);

    RadioGroup = new ButtonGroup ();

    Integer i = 0;
    for (String name: strategies.keySet()) {
      var strBtn = new JRadioButton (name);
      strBtn.setActionCommand(name);
      strBtn.setBounds (540,240 - (30 * i),200,30);
      strBtn.setFont (font1);
      RadioGroup.add(strBtn);
      frame.add(strBtn);
      i++;
    }

    FloorsInput = new JTextField ();
    FloorsInput.setBounds (750,275,100,40);
    FloorsInput.setText ("9");
    FloorsInput.setFont (font);

    LiftsInput = new JTextField ();
    LiftsInput.setBounds (750,385,100,40);
    LiftsInput.setText ("2");
    LiftsInput.setFont (font);

    JLabel label1 = new JLabel ("Кількість поверхів");
    label1.setBounds (540,275,150,40);
    label1.setFont (font1);

    JLabel label2 = new JLabel ("Кількість ліфтів");
    label2.setBounds (540,385,150,40);
    label2.setFont (font1);
    VaitingPassengers = new JLabel ("Кількість пасажирів: ");
    VaitingPassengers.setBounds (540,20,300,40);
    VaitingPassengers.setFont (font1);

    PassengerSpawnTimer = new JTextField ("10");
    PassengerSpawnTimer.setBounds (750,330,100,40);
    PassengerSpawnTimer.setFont (font);

    JLabel PassengerSpawn = new JLabel ("Період генерації пасажира:");
    PassengerSpawn.setBounds (540,330,250,50);
    PassengerSpawn.setFont (font1);
    frame.add (FloorsInput);
    frame.add (LiftsInput);
    frame.add (StartButton);
    frame.add (label1);
    frame.add (label2);
    frame.add (PassengerSpawnTimer);
    frame.add (VaitingPassengers);
    frame.add (PassengerSpawn);
    frame.setVisible(true);
  }

  //  відповідає за запуск  та зупинку симуляції
  class ButtonAction implements ActionListener{
    @Override
    public void actionPerformed(ActionEvent e) {
      if(isRunnned == false){
        try {
          int LiftsCount = Integer.parseInt(LiftsInput.getText ());
          int FloorsCount = Integer.parseInt (FloorsInput.getText ());
          int GenerationTiming = Integer.parseInt (PassengerSpawnTimer.getText ()) * 1000;

          var strategyName = RadioGroup.getSelection().getActionCommand();

          var strategy = strategies.get(strategyName);

          if (strategy != null) {
            building = Builder.init(FloorsCount, GenerationTiming, LiftsCount, strategy);
            building.Start();

            canvas.Init(building);
            Start();
            isRunnned = true;
            StartButton.setText ("Зупинити симуляцію");
          }
        }catch (Exception exc){
          exc.printStackTrace ();
        }
      }else {
        building.Stop ();
        Stop ();
        isRunnned = false;
        StartButton.setText ("Розпочати симуляцію");
      }
    }
  }

  public void Start() {
    if (running.get()) return;
    running.set(true);
    new Thread(() -> {
      while (running.get()) {
        try {
          canvas.paintComponents(canvas.getGraphics());
          Thread.sleep(1000 / 60);
          VaitingPassengers.setText ("Кількість пасажирів: "+ canvas.getPassengersCount ());
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }).start();
  }

  public void Stop() {
    this.running.set(false);
  }
}
