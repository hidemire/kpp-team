package com.company.ui;

import com.company.states.Building;
import com.company.states.Entrance;
import com.company.states.Lift;
import com.company.states.Passenger;

import javax.swing.*;
import java.awt.*;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class UICanvas extends JPanel {
  //private JPanel panel;
  private Building building;

  int FloorWidth = 300;
  int FloorHeight = 0;

  int LiftWidth = 50;
  int LiftHeight = 0;

  public UICanvas(Dimension size) {
    //panel = new JPanel();
    //this.setLayout(new BorderLayout());
    this.setSize(size);
  }

  public void Init(Building building) {
    this.building = building;
    Rectangle r = getBounds();
    int h = r.height;
    int w = r.width;
    //if(building.getFloorsCount ())
    FloorHeight = h / (building.getFloorsCount() + 1);
    if(building.getEntrances ().size () != 0)
      FloorWidth = w / building.getEntrances().size();
    else FloorWidth = w;
    LiftHeight = (int)(0.9 * FloorHeight);
  }

  @Override
  public void paintComponents(Graphics g) {
    super.paintComponents(g);

    //g.setColor (Color.WHITE);
    if (g == null || building == null) return;

    g.clearRect (0,0,getWidth (), getHeight ());
    //g.fillRect (0,0, getWidth (), getHeight ());
    g.setColor (Color.BLACK);

    for(int i =0; i < building.getFloorsCount() + 1; ++i){
      int y = FloorHeight * i;
      g.drawLine (0, y, getWidth (), y);
    }

    for(int i = 1; i < building.getEntrances().size(); ++i){
      int x = FloorWidth * (i);
      g.drawLine (x, 0, x, getHeight ());
    }

    int liftIndex = 0;
    for (Map.Entry<String, Entrance> entrances: building.getEntrances().entrySet()) {
      Lift lift = entrances.getValue().getLift();
      Integer passengersInside = (int) entrances.getValue().getPassengers().stream()
          .filter(p -> p.getValue().getInside()).count();

      int x, y, pos;
      pos = liftIndex;
      x = pos * FloorWidth + (FloorWidth - LiftWidth) / 3;
      y = FloorHeight * (building.getFloorsCount() - lift.getCurrentFloor());
      y += (int) (0.1 * FloorHeight);
      g.fillRect(x, y, LiftWidth, LiftHeight);
      g.drawString(passengersInside.toString (), x - LiftWidth/5, y + FloorHeight/5);
      g.drawString(lift.getStrategy().getCurrentCall().toString(), x - LiftWidth/5, y + (FloorHeight/5)+15);

      liftIndex++;
    }

    int entranceIndex = 0;
    for (Map.Entry<String, Entrance> entrances: building.getEntrances().entrySet()) {
      Entrance entrance = entrances.getValue();

      int[] passengersIndex = new int[building.getFloorsCount() + 1];
      int finalEntranceIndex = entranceIndex;
      entrance.getPassengers().forEach(e -> {
        Passenger passenger = e.getValue();
        if (passenger.getInside()) return;

        g.setColor (Color.orange);
        int x,y,pos, floor;
        pos = finalEntranceIndex;
        floor = passenger.getCurrentFloor();
        x = (((FloorWidth - LiftWidth) / 3 + LiftWidth) + pos * FloorWidth + 5) + passengersIndex[passenger.getCurrentFloor()] * (LiftWidth/3+ LiftWidth/5);
        if(x > (pos+1)*FloorWidth){
          x= (pos+1)*FloorWidth- LiftWidth/3;
        }
        y = FloorHeight * (building.getFloorsCount() - floor) + FloorHeight - (int)(LiftHeight*0.8);
        g.fillRect (x, y, LiftWidth/3, (int)(LiftHeight*0.8));

        g.setColor (Color.BLACK);
        g.drawString(passenger.getRequestedFloor().toString(), x + 5, y + (int)(LiftHeight*0.8*0.8));
        passengersIndex[passenger.getCurrentFloor()]++;
      });

      entranceIndex++;
    }
  }
  public int getPassengersCount(){
    int count = 0;
    for(Map.Entry<String, Entrance> entrances: building.getEntrances().entrySet()){
     count += entrances.getValue().getPassengersCount ();
    }
    return count;
  }
}
