package com.company.states;

import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class Lift extends BaseState {
  private Entrance entrance;

  private ILiftStrategy strategy;

  private Integer loadCapacity;
  private Integer maxPassengersLoad;

  private LinkedBlockingQueue<Map.Entry<Integer, Passenger>> calls = new LinkedBlockingQueue<>();
  private AtomicInteger currentFloor = new AtomicInteger(1);
  private AtomicBoolean isLocked = new AtomicBoolean(false);

  Runnable payload = () -> {
    strategy.execute(this);
  };

  public Lift(Integer maxPassengersLoad, Integer loadCapacity, ILiftStrategy strategy) {
    super(1000);
    this.maxPassengersLoad = maxPassengersLoad;
    this.loadCapacity = loadCapacity;
    this.strategy = strategy;
  }

  public void Start(Entrance entrance) {
    System.out.println(String.format("Lift start entrance name - %s", entrance.getName()));

    this.entrance = entrance;
    super.setPayload(payload);
    super.Start();
  }

  synchronized public Boolean TryEnter(Passenger passenger) {
      System.out.println(String.format("Try enter - lift %s, passenger %s", this.entrance.getName(), passenger));
      if (calls.size() < maxPassengersLoad && !isLocked.get()) {
        Map.Entry<Integer, Passenger> call = Map.entry(passenger.getRequestedFloor(), passenger);

        System.out.println(call);
        calls.add(call);
        return true;
      }
      return false;
  }

  public void MoveTo(Integer floor, List<Passenger> passengers) {
    if (!super.running.get() || floor < 1 || floor > entrance.getBuilding().getFloorsCount()) return;
    System.out.println(String.format("Lift move to %d floor", floor));

    currentFloor.set(floor);
    synchronized (passengers) {
      passengers.forEach(p -> p.setCurrentFloor(floor));
    }

    calls.removeIf(integerPassengerEntry -> integerPassengerEntry.getKey() == floor);
  }

  public Integer getCurrentFloor() {
    return currentFloor.get();
  }

  public Queue<Map.Entry<Integer, Passenger>> getCalls() {
    return calls;
  }

  public Entrance getEntrance() {
    return entrance;
  }

  public Boolean getIsLocked() {
    return isLocked.get();
  }

  public ILiftStrategy getStrategy() {
    return strategy;
  }

  public void setIsLocked(Boolean isLocked) {
    this.isLocked.set(isLocked);
  }
}
