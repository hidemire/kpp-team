package com.company.states;

import java.util.concurrent.atomic.AtomicBoolean;

public class BaseState {
  protected AtomicBoolean running = new AtomicBoolean(false);
  private Runnable payload;

  private Integer timeout;

  public BaseState(Integer timeout) {
    this.timeout = timeout;
  }

  void setPayload(Runnable payload) {
    this.payload = payload;
  }

  protected void Start() {
    if (running.get()) return;
    running.set(true);

    new Thread(() -> {
      while (running.get()) {
        payload.run();

        try {
          Thread.sleep(this.timeout);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }).start();
  }

  protected void Stop() {
    running.set(false);
  }
}
