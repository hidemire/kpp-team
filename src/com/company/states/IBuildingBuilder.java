package com.company.states;

import com.company.strategy.BaseLiftStrategy;

public interface IBuildingBuilder {
  Building init(int floorsCount, int generationPeriod, int liftCount, Class<? extends BaseLiftStrategy> strategy);
}
