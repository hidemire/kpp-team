package com.company.states;

public interface ILiftStrategy {
  String getName();
  void execute(Lift lift);
  Integer getCurrentCall();
}
