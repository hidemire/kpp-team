package com.company.states;

public class Passenger extends BaseState {
  private Integer weight;
  private Integer currentFloor;
  private Integer requestedFloor;
  private Entrance entrance;

  private Boolean isInside = false;

  Runnable payload = () -> {
    if (currentFloor != requestedFloor && entrance.getLift().getCurrentFloor() == currentFloor && !isInside) {
      System.out.println(
          String.format(
              "Passenger try enter: entrance - %s, floor - %d, requested floor - %d",
              entrance.getName(),
              this.currentFloor,
              this.requestedFloor
          )
      );

      isInside = entrance.getLift().TryEnter(this);

      System.out.println(String.format("Is inside %b", isInside));
      return;
    }

    if (currentFloor == requestedFloor) {
      this.isInside = false;
    }
  };

  public Passenger(Integer weight,
                   Integer currentFloor,
                   Integer requestedFloor,
                   Entrance entrance) {
    super(400);
    this.weight = weight;
    this.currentFloor = currentFloor;
    this.requestedFloor = requestedFloor;
    this.entrance = entrance;
  }

  public void Start() {
    System.out.println(
        String.format(
            "Passenger start: entrance - %s, floor - %d, requested floor - %d",
            entrance.getName(),
            this.currentFloor,
            this.requestedFloor
        )
    );

    super.setPayload(payload);
    super.Start();
  }

  public Integer getRequestedFloor() {
    return requestedFloor;
  }

  public Integer getCurrentFloor() {
    return currentFloor;
  }

  public Boolean getInside() {
    return isInside;
  }

  public void setCurrentFloor(Integer currentFloor) {
    this.currentFloor = currentFloor;
  }
}
