package com.company.states;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class Entrance extends BaseState {
  private Lift lift;
  private String name;
  private Building building;

  private List<Map.Entry<Integer, Passenger>> passengers = Collections.synchronizedList(new ArrayList<>());

  Runnable payload = () -> {
    clearPassengersOnRequestedFloor();

    Integer weight = ThreadLocalRandom.current().nextInt(40, 90);
    Integer currentFloor = ThreadLocalRandom.current().nextInt(1, this.building.getFloorsCount() + 1);
    Integer requestedFloor = currentFloor;

    while (requestedFloor == currentFloor) {
      requestedFloor = ThreadLocalRandom.current().nextInt(1, this.building.getFloorsCount() + 1);
    }

    Passenger passenger = new Passenger(weight, currentFloor, requestedFloor, this);

    System.out.println(String.format("Add passenger: entrance - %s, passenger", this.name, passenger));

    passengers.add(Map.entry(currentFloor, passenger));
    passenger.Start();

    System.out.println(String.format("Waiting passengers: count - %d", passengers.size()));
  };

  public Entrance(Lift lift, Integer spawnTimeout, String name, Building building) {
    super(spawnTimeout);
    this.lift = lift;
    this.name = name;
    this.building = building;
  }

  public String getName() {
    return this.name;
  }

  public Lift getLift() {
    return lift;
  }

  public Building getBuilding() {
    return building;
  }

  public List<Map.Entry<Integer, Passenger>> getPassengers() {
    return passengers;
  }

  public List<Passenger> getPassengersInsideLift() {
    return passengers.stream()
        .filter(p -> p.getValue().getInside())
        .map(p -> p.getValue())
        .collect(Collectors.toList());
  }

  public Map.Entry<Integer, Passenger> getNextPassenger() {
    var passengers = this.passengers.stream()
        .filter(p -> !p.getValue().getInside()).collect(Collectors.toList());

    if (passengers.size() == 0)  return null;
    return passengers.get(0);
  }
  public int getPassengersCount(){
    return (int) passengers.stream().count ();
  }
  public void Start() {
    System.out.println(String.format("Entrance start: name - %s", this.name));

    super.setPayload(payload);
    super.Start();
    this.lift.Start(this);
  }

  public void clearPassengersOnRequestedFloor() {
    passengers.removeIf(p -> p.getValue().getRequestedFloor() == p.getValue().getCurrentFloor());
  }

  public void Stop() {
    super.Stop();
    this.lift.Stop();

    synchronized (passengers) {
      passengers.forEach(p -> p.getValue().Stop());
    }
  }
}
