package com.company.states;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Building {
  private Integer floorsCount;
  private Map<String, Entrance> entrances = Collections.synchronizedMap(new HashMap<>());

  public Building(Integer floorsCount) {
    this.floorsCount = floorsCount;
  }

  public void addEntrance(Lift lift, Integer spawnTimeout, String name) {
    Entrance entrance = new Entrance(lift, spawnTimeout, name, this);
    entrances.put(entrance.getName(), entrance);
  }

  public void Start() {
    entrances.values().forEach(e -> e.Start());
  }

  public void Stop() {
    entrances.values().forEach(e -> e.Stop());
  }

  public Integer getFloorsCount() {
    return floorsCount;
  }

  public Map<String, Entrance> getEntrances() {
    return entrances;
  }
}
