package com.company;

import com.company.states.*;
import com.company.strategy.BaseLiftStrategy;
import com.company.strategy.LiftLoadStrategy;
import com.company.strategy.OptimalLiftStrategy;
import com.company.ui.UI;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Map<String, Class<? extends BaseLiftStrategy>> strategies = new HashMap<>();

        strategies.put("Оптимальна стратегія", OptimalLiftStrategy.class);
        strategies.put("Повільна стратегія", LiftLoadStrategy.class);

        IBuildingBuilder builder = (floorsCount, generationPeriod, liftCount, strategy) -> {
            Building building = new Building (floorsCount);
            for (int i =0; i < liftCount; ++i){
                try {
                    building.addEntrance (
                        new Lift (4,200, strategy.getDeclaredConstructor().newInstance()),
                        generationPeriod,
                        ""+i
                    );
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
            }
            return building;
        };

        UI ui = new UI(builder, strategies);

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            ui.Stop();
        }));
    }
}
