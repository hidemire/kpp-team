package com.company.controlers;

import java.util.concurrent.atomic.AtomicBoolean;

public abstract class ThreadController {
  private AtomicBoolean running = new AtomicBoolean(false);
  private Runnable payload;

  private Integer timeout;

  public ThreadController(Integer timeout) {
    this.timeout = timeout;
  }

  void setPayload(Runnable payload) {
    this.payload = payload;
  }

  protected void Start() {
      new Thread(() -> {
        while (running.get()) {
          payload.run();

          try {
            Thread.sleep(this.timeout);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
      }).start();
  }

  protected void Stop() {
    running.set(false);
  }
}
